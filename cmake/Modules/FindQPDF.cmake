# - Find QPDF
# This module finds if QPDF is installed and determines where the executables
# are. This code sets the following variables:
#
# QPDF_FOUND:      whether context has been found or not
# QPDF_EXECUTABLE: full path to the context executable if it has been found
# QPDF_VERSION:    version number of the available context

find_program(QPDF_EXECUTABLE
  NAMES qpdf)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(QPDF DEFAULT_MSG QPDF_EXECUTABLE)

mark_as_advanced(QPDF_EXECUTABLE)

if(NOT QPDF_FOUND)
    message(WARNING "QPDF executable not found!")
    message(WARNING "Linearization will not be performed.")
endif(NOT QPDF_FOUND)
